import Vue from 'vue'
import { rawToDate } from './utils/rawToDate'
import { mdToHtml } from './utils/mdToHtml'
import { ellipsis } from './utils/ellipsis'
import { capitalize } from './utils/capitalize'
import { formatDate } from './utils/formatDate'

Vue.filter('capitalize', capitalize)
Vue.filter('rawToDate', rawToDate)
Vue.filter('ellipsis', ellipsis)
Vue.filter('marked', mdToHtml)
Vue.filter('formatDate', formatDate)
