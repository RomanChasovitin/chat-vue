import Vue from 'vue'

import Buefy from 'buefy'
import 'buefy/dist/buefy.css'

import VCalendar from 'v-calendar'

Vue.use(Buefy)
Vue.use(VCalendar)

Vue.config.productionTip = false
