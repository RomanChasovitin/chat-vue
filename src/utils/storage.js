/* eslint-disable max-len */
export const USERS_KEY = 'USERS'
export const MY_TASKS_KEY = 'MY_TASKS'
export const USERS_TASKS_KEY = 'USERS_TASKS'
export const MARKET_DATA_KEY = 'MARKET_DATA'

const initialUsers = [
  {
    id: 1,
    name: 'Elon Musk',
    messages: [
      {
        id: 2,
        content: `Great band, too bad they broke up
          ![Image](https://pbs.twimg.com/media/Ev6Tz76UcAEFWp2?format=jpg&name=small)
        `,
        date: 1615018120725,
        isMy: false,
        from: 'twitter',
        unread: true,
      },
      {
        id: 3,
        content: `Carnot efficiency of the Universe?
        `,
        date: 1615018130725,
        isMy: false,
        from: 'twitter',
        unread: true,
      },
      {
        id: 4,
        content: `![Image](https://pbs.twimg.com/media/Ev9dXccUYAEAAdz?format=jpg&name=small)
        `,
        date: 1615018140725,
        isMy: false,
        from: 'twitter',
        unread: true,
      },
      {
        id: 1,
        content: `Availability varies by region due to regulatory approval delays and/or Tesla internal development & testing. 
          
          Note: word “Beta” is used to reduce complacency in usage & set expectations appropriately. All software is first tested internally by Tesla simulation & QA drive teams.
        `,
        date: 1615018110725,
        isMy: false,
        from: 'twitter',
        unread: true,
      },
    ],
    avatar: 'https://www.biography.com/.image/ar_1:1%2Cc_fill%2Ccs_srgb%2Cg_face%2Cq_auto:good%2Cw_300/MTY2MzU3Nzk2OTM2MjMwNTkx/elon_musk_royal_society.jpg',
    info: {
      books: `Oh, I very like to read some books. I like only professional books
        
        My favourite professional books is:
        * [Zero to One: Notes on Startups, or How to Build the Future](https://www.amazon.com/gp/product/0804139296/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=farnamstreet-20&creative=9325&linkCode=as2&creativeASIN=0804139296&linkId=ffa614bf841a3ed645358c7bbac9494b)
        * [Superintelligence: Paths, Dangers, Strategies](https://www.amazon.com/gp/product/0199678111/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=farnamstreet-20&creative=9325&linkCode=as2&creativeASIN=0199678111&linkId=0743bb3f0347c472a01bef615d8fa602)
        * [Howard Hughes: His Life and Madness](https://www.amazon.com/gp/product/0393326020/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=farnamstreet-20&creative=9325&linkCode=as2&creativeASIN=0393326020&linkId=966c405d592a0f303dfcaa07dfe2ecb0)
        * [The Hitchhiker’s Guide to the Galaxy](https://www.amazon.com/gp/product/0345391802/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=farnamstreet-20&creative=9325&linkCode=as2&creativeASIN=0345391802&linkId=c14a0d46f50ef0ff799d00155cf30b8d)
        
        And my favourite fiction books is:
        * [The Lord of the Rings](https://www.amazon.com/gp/product/0544003411/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=farnamstreet-20&creative=9325&linkCode=as2&creativeASIN=0544003411&linkId=ccc2becf7a30e1ea5dc7f862f4153ad5)
        * [Benjamin Franklin: An American Life](https://www.amazon.com/gp/product/074325807X/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=farnamstreet-20&creative=9325&linkCode=as2&creativeASIN=074325807X&linkId=4ec9f191a4cddf21fc43de253d6a6415)
      `,
      schedule: `My one day in life:

        * 7:00 AM - Wakes up. Usually skips breakfast, sometimes has an omelet with coffee
        
        * 10:00 AM - Making morning phone calls. Normal calls to journalists, job candidates or conference calls about SpaceX and Tesla
        
        * 10:45 AM - Has meetings to discuss the status of the projects of SpaceX and Tesla
        
        * 11:30 AM - Wolfs down lunch in a 5-minute span during a meeting
        
        * 12:30 PM - Technical meetings for Tesla and SpaceX
        
        * 04:30 PM - Goes for his daily walk of the factory floor
        
        * 07:00 PM - Has a couple of Job interviews
        
        * 01:00 AM - Check emails and goes to bed
      `,
      music: `I like to listen music on Spotify. It's the best music service ever.
      I have a couple of playlists for different moods.

      * [Here you can listen to my favorite playlist](https://open.spotify.com/playlist/73tqfhWVBUq4Rup1mdEXel?si=TmEuFo3CTO6pdlhWB08U5w&nd=1)
      `,
      food: `My favourite food is:

      * For breakfast, I prefer to eat an omelet with a black cup of coffee

      * For lunch, I like to eat some Italian pasta and drink some juice

      * For dinner, I always eat beef steak with a cup of wine

      * Also I like to eat healthy snacks during a day, nuts raising etc.
      `,
      hobby: `I like to fun sometimes. For example, I like to record some songs or fun video.
      
      Also I like to watch anime and love some anime movies. 
      
      Sometimes I like to tweet some fun stories in my twitter.
      `,
    },
  },
  {
    id: 2,
    name: 'Cristiano Ronaldo',
    messages: [
      {
        id: 2,
        content: `Tomorrow we have a very importante game against a very strong team and I can only hope that it may be the beggining of the long walk we want to take until the final. 
        Respect for the opponent, ambition for the victory and 100% focus on our goals. Let’s go, guys! Fino Alla Fine!
        ![Image](https://pbs.twimg.com/media/EuWcH-mXMAYpPXN?format=jpg&name=small)
        `,
        date: 1615018120725,
        isMy: false,
        from: 'twitter',
        unread: true,
      },
      {
        id: 3,
        content: `Recovery time!
        ![Image](https://pbs.twimg.com/media/Evk8_OyXMAAumDl?format=jpg&name=small)
        `,
        date: 1615018130725,
        isMy: false,
        from: 'twitter',
        unread: true,
      },
      {
        id: 4,
        content: `Did you grab a pair?! Can’t wait to see what all of you have created⚡

        Only a few days left to customize my CR7 AF1 with [#NikeByYou](https://twitter.com/hashtag/NikeByYou?src=hashtag_click)
        Available now [https://nike.app.link/vEzGHsht4cb](https://nike.app.link/vEzGHsht4cb)
        [#CR7ByYou](https://twitter.com/hashtag/CR7ByYou?src=hashtag_click) [#TeamNike](https://twitter.com/hashtag/TeamNike?src=hashtag_click)
        ![Image](https://pbs.twimg.com/media/Evolz1VXAAAOwei?format=jpg&name=small)
        `,
        date: 1615018140725,
        isMy: false,
        from: 'twitter',
        unread: true,
      },
    ],
    avatar: 'https://pbs.twimg.com/profile_images/1157313327867092993/a09TxL_1_400x400.jpg',
    info: {
      books: `Oh, I very like to read some books. I like only professional books
        
        My favourite professional books is:
        * [Zero to One: Notes on Startups, or How to Build the Future](https://www.amazon.com/gp/product/0804139296/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=farnamstreet-20&creative=9325&linkCode=as2&creativeASIN=0804139296&linkId=ffa614bf841a3ed645358c7bbac9494b)
        * [Superintelligence: Paths, Dangers, Strategies](https://www.amazon.com/gp/product/0199678111/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=farnamstreet-20&creative=9325&linkCode=as2&creativeASIN=0199678111&linkId=0743bb3f0347c472a01bef615d8fa602)
        * [Howard Hughes: His Life and Madness](https://www.amazon.com/gp/product/0393326020/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=farnamstreet-20&creative=9325&linkCode=as2&creativeASIN=0393326020&linkId=966c405d592a0f303dfcaa07dfe2ecb0)
        * [The Hitchhiker’s Guide to the Galaxy](https://www.amazon.com/gp/product/0345391802/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=farnamstreet-20&creative=9325&linkCode=as2&creativeASIN=0345391802&linkId=c14a0d46f50ef0ff799d00155cf30b8d)
        
        And my favourite fiction books is:
        * [The Lord of the Rings](https://www.amazon.com/gp/product/0544003411/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=farnamstreet-20&creative=9325&linkCode=as2&creativeASIN=0544003411&linkId=ccc2becf7a30e1ea5dc7f862f4153ad5)
        * [Benjamin Franklin: An American Life](https://www.amazon.com/gp/product/074325807X/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=farnamstreet-20&creative=9325&linkCode=as2&creativeASIN=074325807X&linkId=4ec9f191a4cddf21fc43de253d6a6415)
      `,
      schedule: `My one day in life:

        * 7:00 AM - Wakes up. Usually skips breakfast, sometimes has an omelet with coffee
        
        * 10:00 AM - Making morning phone calls. Normal calls to journalists, job candidates or conference calls about SpaceX and Tesla
        
        * 10:45 AM - Has meetings to discuss the status of the projects of SpaceX and Tesla
        
        * 11:30 AM - Wolfs down lunch in a 5-minute span during a meeting
        
        * 12:30 PM - Technical meetings for Tesla and SpaceX
        
        * 04:30 PM - Goes for his daily walk of the factory floor
        
        * 07:00 PM - Has a couple of Job interviews
        
        * 01:00 AM - Check emails and goes to bed
      `,
      music: `I like to listen music on Spotify. It's the best music service ever.
      I have a couple of playlists for different moods.

      * [Here you can listen to my favorite playlist](https://open.spotify.com/playlist/73tqfhWVBUq4Rup1mdEXel?si=TmEuFo3CTO6pdlhWB08U5w&nd=1)
      `,
      food: `My favourite food is:

      * For breakfast, I prefer to eat an omelet with a black cup of coffee

      * For lunch, I like to eat some Italian pasta and drink some juice

      * For dinner, I always eat beef steak with a cup of wine

      * Also I like to eat healthy snacks during a day, nuts raising etc.
      `,
      hobby: `I like to fun sometimes. For example, I like to record some songs or fun video.
      
      Also I like to watch anime and love some anime movies. 
      
      Sometimes I like to tweet some fun stories in my twitter.
      `,
    },
  },
  {
    id: 3,
    name: 'Joe Biden',
    messages: [
      {
        id: 2,
        content: `The right to vote is sacred and fundamental — and H.R. 1 is urgently needed to protect that right, to safeguard the integrity of our elections, and to repair and strengthen our democracy. 
        I look forward to signing it into law after it has passed through the legislative process.`,
        date: 1615018120725,
        isMy: false,
        from: 'twitter',
        unread: true,
      },
      {
        id: 3,
        content: `With the Johnson & Johnson vaccine, we now have a third safe and effective COVID-19 vaccine being administered across the country. 
        I’m grateful these folks are stepping up and getting vaccinated — and I encourage every American to do the same when it’s their turn.`,
        date: 1615018130725,
        isMy: false,
        from: 'twitter',
        unread: true,
      },
      {
        id: 4,
        content: `When I took office, I promised help was on the way. 
        Thanks to [@SenSchumer](https://twitter.com/SenSchumer) and Senate Democrats, we've taken one more giant step forward in delivering on that promise.
        I hope the American Rescue Plan receives a quick passage in the House so it can be sent to my desk to be signed.`,
        date: 1615018140725,
        isMy: false,
        from: 'twitter',
        unread: true,
      },
    ],
    avatar: 'https://pbs.twimg.com/profile_images/1308769664240160770/AfgzWVE7_400x400.jpg',
    info: {
      books: `Oh, I very like to read some books. I like only professional books
        
        My favourite professional books is:
        * [Zero to One: Notes on Startups, or How to Build the Future](https://www.amazon.com/gp/product/0804139296/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=farnamstreet-20&creative=9325&linkCode=as2&creativeASIN=0804139296&linkId=ffa614bf841a3ed645358c7bbac9494b)
        * [Superintelligence: Paths, Dangers, Strategies](https://www.amazon.com/gp/product/0199678111/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=farnamstreet-20&creative=9325&linkCode=as2&creativeASIN=0199678111&linkId=0743bb3f0347c472a01bef615d8fa602)
        * [Howard Hughes: His Life and Madness](https://www.amazon.com/gp/product/0393326020/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=farnamstreet-20&creative=9325&linkCode=as2&creativeASIN=0393326020&linkId=966c405d592a0f303dfcaa07dfe2ecb0)
        * [The Hitchhiker’s Guide to the Galaxy](https://www.amazon.com/gp/product/0345391802/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=farnamstreet-20&creative=9325&linkCode=as2&creativeASIN=0345391802&linkId=c14a0d46f50ef0ff799d00155cf30b8d)
        
        And my favourite fiction books is:
        * [The Lord of the Rings](https://www.amazon.com/gp/product/0544003411/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=farnamstreet-20&creative=9325&linkCode=as2&creativeASIN=0544003411&linkId=ccc2becf7a30e1ea5dc7f862f4153ad5)
        * [Benjamin Franklin: An American Life](https://www.amazon.com/gp/product/074325807X/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=farnamstreet-20&creative=9325&linkCode=as2&creativeASIN=074325807X&linkId=4ec9f191a4cddf21fc43de253d6a6415)
      `,
      schedule: `My one day in life:

        * 7:00 AM - Wakes up. Usually skips breakfast, sometimes has an omelet with coffee
        
        * 10:00 AM - Making morning phone calls. Normal calls to journalists, job candidates or conference calls about SpaceX and Tesla
        
        * 10:45 AM - Has meetings to discuss the status of the projects of SpaceX and Tesla
        
        * 11:30 AM - Wolfs down lunch in a 5-minute span during a meeting
        
        * 12:30 PM - Technical meetings for Tesla and SpaceX
        
        * 04:30 PM - Goes for his daily walk of the factory floor
        
        * 07:00 PM - Has a couple of Job interviews
        
        * 01:00 AM - Check emails and goes to bed
      `,
      music: `I like to listen music on Spotify. It's the best music service ever.
      I have a couple of playlists for different moods.

      * [Here you can listen to my favorite playlist](https://open.spotify.com/playlist/73tqfhWVBUq4Rup1mdEXel?si=TmEuFo3CTO6pdlhWB08U5w&nd=1)
      `,
      food: `My favourite food is:

      * For breakfast, I prefer to eat an omelet with a black cup of coffee

      * For lunch, I like to eat some Italian pasta and drink some juice

      * For dinner, I always eat beef steak with a cup of wine

      * Also I like to eat healthy snacks during a day, nuts raising etc.
      `,
      hobby: `I like to fun sometimes. For example, I like to record some songs or fun video.
      
      Also I like to watch anime and love some anime movies. 
      
      Sometimes I like to tweet some fun stories in my twitter.
      `,
    },
  },
]

const initialMyTasks = [
  {
    id: 1,
    from: 'Cristiano Ronaldo',
    text: 'Football match',
    date: new Date('March 15, 2021').getTime(),
  },
  {
    id: 2,
    from: 'Elon Musk',
    text: 'SpaceX conference',
    date: new Date('March 19, 2021').getTime(),
  },
  {
    id: 3,
    from: 'Cristiano Ronaldo',
    text: 'Workout',
    date: new Date('March 23, 2021').getTime(),
  },
  {
    id: 4,
    from: 'Elon Musk',
    text: 'Tesla showroom',
    date: new Date('March 27, 2021').getTime(),
  },
]

const initialUserTasks = [
  {
    id: 32,
    from: 'Cristiano Ronaldo',
    text: 'Football match',
    date: new Date('March 26, 2021').getTime(),
  },
  {
    id: 45,
    from: 'Elon Musk',
    text: 'SpaceX conference',
    date: new Date('March 24, 2021').getTime(),
  },
  {
    id: 81,
    from: 'Cristiano Ronaldo',
    text: 'Workout',
    date: new Date('March 22, 2021').getTime(),
  },
  {
    id: 49,
    from: 'Elon Musk',
    text: 'Tesla showroom',
    date: new Date('March 19, 2021').getTime(),
  },
  {
    id: 11,
    from: 'Cristiano Ronaldo',
    text: 'Football match',
    date: new Date('March 17, 2021').getTime(),
  },
  {
    id: 21,
    from: 'Elon Musk',
    text: 'SpaceX conference',
    date: new Date('March 30, 2021').getTime(),
  },
  {
    id: 31,
    from: 'Cristiano Ronaldo',
    text: 'Workout',
    date: new Date('March 20, 2021').getTime(),
  },
  {
    id: 41,
    from: 'Elon Musk',
    text: 'Tesla showroom',
    date: new Date('March 14, 2021').getTime(),
  },
  {
    id: 11,
    from: 'Cristiano Ronaldo',
    text: 'Football match',
    date: new Date('March 16, 2021').getTime(),
  },
  {
    id: 12,
    from: 'Elon Musk',
    text: 'SpaceX conference',
    date: new Date('March 20, 2021').getTime(),
  },
  {
    id: 13,
    from: 'Cristiano Ronaldo',
    text: 'Workout',
    date: new Date('March 24, 2021').getTime(),
  },
  {
    id: 14,
    from: 'Elon Musk',
    text: 'Tesla showroom',
    date: new Date('March 28, 2021').getTime(),
  },
]

const initialMarkedData = [
  {
    id: 1,
    name: 'Donald Trump',
    price: '29.99$',
    available: true,
    avatar: 'https://specials-images.forbesimg.com/imageserve/5f88e1c6f5fe1c82b41072c9/960x0.jpg?fit=scale',
  },
  {
    id: 2,
    name: 'LeBron James',
    price: '19.99$',
    available: true,
    avatar: 'https://pyxis.nymag.com/v1/imgs/847/0f7/504c63a03d8a751a5cbeda0bc064306bb4-lebron-james.rsquare.w1200.jpg',
  },
  {
    id: 3,
    name: 'Leonardo Di Caprio',
    price: '4.99$',
    available: false,
    avatar: 'https://in.bmscdn.com/iedb/artist/images/website/poster/large/leonardo-dicaprio-1273-06-05-2020-06-55-21.jpg?1',
  },
  {
    id: 4,
    name: 'Vladimir Putin',
    price: '7.99$',
    available: true,
    avatar: 'https://s0.rbk.ru/v6_top_pics/media/img/1/69/755945509445691.jpg',
  },
  {
    id: 5,
    name: 'Drake',
    price: '99.99$',
    available: true,
    avatar: 'https://pyxis.nymag.com/v1/imgs/bcc/37a/e22113c0d1fa59b710e5e4d643e2e65110-drake.rsquare.w1200.jpg',
  },
]

const initialData = {
  [USERS_KEY]: initialUsers,
  [MY_TASKS_KEY]: initialMyTasks,
  [USERS_TASKS_KEY]: initialUserTasks,
  [MARKET_DATA_KEY]: initialMarkedData,
}

export const load = KEY => {
  try {
    const data = JSON.parse(localStorage.getItem(KEY)) || initialData[KEY]
    return data
  } catch (err) {
    return initialData[KEY]
  }
}

export const save = (data, KEY) => {
  try {
    localStorage.setItem(KEY, JSON.stringify(data))
    return true
  } catch (err) {
    return false
  }
}
