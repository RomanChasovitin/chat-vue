export const getHighlightByUser = user => {
  const defaultHighlight = 'purple'

  const highlights = {
    'Elon Musk': 'red',
    'Crisitiano Ronaldo': 'green',
  }
  return highlights[user] || defaultHighlight
}
