import marked from 'marked'

export const mdToHtml = value => marked.parseInline(value)
