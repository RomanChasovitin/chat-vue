export const questions = {
  books: ['What are your favourite books?', 'Do you like to read?', 'What is your favourite professional literature?', 'What is your favourite fiction books?'],
  schedule: ['What are you doing in the morning?', 'What are you doing in the evening?', 'What is your daily routine?', 'What are you doing before bed?', 'What is your best time to relax?'],
  music: ['What is you favourite song?', 'What is your favorite genre of music?', 'Which music platform are you use?', 'Do you like rock?'],
  food: ['What do you eat for breakfast?', 'What do you eat for diner?', 'Do you like fast food?', 'Do you like healthy food?'],
  hobby: ['What is your favourite hobby?', 'What do you like to do during free time?', 'Do you like to fun?'],
}

export const getQuestionType = question => {
  const type = Object.entries(questions).reduce((acc, [category, q]) => (q.includes(question) ? category : acc), null)
  return type
}

export const getSuggestion = inputString => {
  if (!inputString) return []
  const questionsArray = Object.values(questions).flat()
  const results = questionsArray.filter(question => question.toLowerCase().includes(inputString.toLowerCase()))
  return results.slice(0, 4)
}
