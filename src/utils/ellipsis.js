export const ellipsis = (
  value,
  len = 20,
  useEllipsis = true,
) => {
  if (value.length < len) return value
  if (useEllipsis) return `${value.slice(0, len - 3).trim()}...`
  return value.slice(0, len).trim()
}
