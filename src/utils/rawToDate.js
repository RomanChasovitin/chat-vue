import moment from 'moment'

export const rawToDate = (unix, isFull = false) => {
  const date = moment(unix)

  const isToday = moment().diff(date, 'days') === 0
  const isWeek = moment().diff(date, 'week') === 0

  if (isToday) return date.format('LT')
  if (isWeek && isFull) return date.format('ddd, LT')
  if (isWeek) return date.format('ddd')
  if (isFull) return date.format('LT, dd mmm')
  return date.format('dd mmm')
}
