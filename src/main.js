import Vue from 'vue'
import './filters'
import './lib'
import './assets/styles/index.scss'

import App from './App.vue'

new Vue({
  render: h => h(App),
}).$mount('#app')
